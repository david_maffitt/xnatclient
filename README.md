# XNATClient #

Java and Spring -based library to interact with XNAT.

### Usage ###

This has the XNAT schema as part of the source and builds its own JAXB classes for XNAT objects. You need to
verify that the schema you build the client with match the schema used in the XNAT instance.

### Build From Source ###

* clone this repo.
* `mvn clean install`