package org.nrg.xnatClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@ComponentScan
@PropertySource("xnat.properties")
public class XNATTestContextConfiguration {

    // env will have the properties defined in property source files.
    @Autowired
    private Environment env;

    @Autowired
    public DataSource xnatDataSource;

    @Bean(name="xnatClient")
    public XNATClient createXNATClient() throws IOException {
        return new XNATClient( xnatDataSource, env.getProperty( "rest.url"), env.getProperty( "rest.user"), env.getProperty( "rest.password"));
    }

    @Bean
    public DataSource createDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName( env.getProperty( "jdbc.driver"));
        dataSource.setUrl( env.getProperty( "jdbc.url"));
        dataSource.setUsername( env.getProperty( "jdbc.user"));
        dataSource.setPassword( env.getProperty( "jdbc.password"));

        return dataSource;
    }

}