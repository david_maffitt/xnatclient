package org.nrg.xnatClient;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.nrg.catalog.Catalog;
import org.nrg.catalog.DcmCatalog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { XNATTestContextConfiguration.class })
public class XNATClientTest {

    @Autowired
    private XNATClient xnatClient;

    @Test
    public void getScanCatalog() {
        try {

//            String projectId = "BC_1";
//            String subjectLabel = "bc1uivv";
//            String sessionLabel = "bc1uivv_mg0000_1";
//            String scanId = "71100000-MG1";

            String projectId = "xdc";
            String subjectLabel = "sample1";
            String sessionLabel = "sample1_MR_1";
            String scanId = "4";

            Catalog catalog = xnatClient.getScanCatalog( projectId, subjectLabel, sessionLabel, scanId);
            if( catalog instanceof DcmCatalog) {

//                assertEquals("1.2.840.113654.2.45.6243.13915564127457094585395320378648493853", ((DcmCatalog) catalog).getUID());
                assertEquals("1.3.12.2.1107.5.2.32.35177.3.2006121409284535196417894.0.0.0", ((DcmCatalog) catalog).getUID());
            }
            else {
                fail( "Why a Catalog and not a DCMCatalog?");
            }

        } catch (Exception e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    public void getSubjectLabelsTest() {
        try {
            String projectId = "xdc";

            List<String> subjectLabels = xnatClient.getSubjectLabels( projectId);
            assertEquals("", "");

        } catch (Exception e) {
            fail( "Unexpected exception: " + e);
        }
    }

}
