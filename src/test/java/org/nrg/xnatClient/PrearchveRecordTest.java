package org.nrg.xnatClient;

import org.junit.Test;
import org.nrg.xnatClient.datamodels.PrearchiveRecord;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PrearchveRecordTest {
    @Test
    public void csvConstructor() {
        try {

            String csvLine = "BC_1,20181018_220617337,2018-10-18 22:11:58.089,2018-10-18 22:06:17.337,1982-11-17 00:00:00.0,,bc1mpcn,bc1mpcn_mg4703_1,bc1mpcn_mg4703_1,1.2.840.113654.2.45.6243.290130230435728745554401571256622710352,READY,/prearchive/projects/BC_1/20181018_220617337/bc1mpcn_mg4703_1,Manual,false,false,,,,";

            PrearchiveRecord prearchiveRecord = new PrearchiveRecord( csvLine);

            assertEquals("BC_1", prearchiveRecord.getProjectID());

        } catch (Exception e) {
            fail( "Unexpected exception: " + e);
        }
    }
}
