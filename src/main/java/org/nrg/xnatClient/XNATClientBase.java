package org.nrg.xnatClient;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
//import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.net.HttpCookie;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class XNATClientBase {
    protected JdbcTemplate jdbcTemplate;
    protected RestTemplate restTemplate;

    public XNATClientBase(DataSource dataSource, String restURL, String restUser, String restPwd) throws IOException {
        this.jdbcTemplate = createJdbcTemplate( dataSource);
        this.restTemplate = createRestTemplate( restURL, restUser, restPwd);
    }

    private JdbcTemplate createJdbcTemplate( DataSource xnatDataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource( xnatDataSource);
        return jdbcTemplate;
    }

    private RestTemplate createRestTemplate( String restURL, String restUser, String restPwd) throws IOException {

        XNATSession xnatSession = getSession( restURL, restUser, restPwd);

        RestTemplate restTemplate =  (new RestTemplateBuilder())
                .rootUri( restURL)
                .build();
        restTemplate.setInterceptors( Arrays.asList(new JSessionAuthInterceptor( xnatSession.getJSessionID())));
        restTemplate.setMessageConverters( getMessageConverters());
//        restTemplate.setRequestFactory( new MyCustomClientHttpRequestFactory());
        return restTemplate;
    }

    private XNATSession getSession( String restURL, String restUser, String restPwd) throws IOException {
        XNATSession xnatSession = new XNATSession();

        RestTemplate restTemplate =  (new RestTemplateBuilder())
                .rootUri( restURL)
                .build();

        // Falling back to spring boot 1.5.11 for spring framework 4.x used by nrg.framework.
//        restTemplate.setInterceptors(Arrays.asList(new BasicAuthenticationInterceptor( restUser, restPwd)));
        restTemplate.setInterceptors(Arrays.asList(new BasicAuthorizationInterceptor( restUser, restPwd)));

//        restTemplate.setMessageConverters( getMessageConverters());
        // for https
//        restTemplate.setRequestFactory( new MyCustomClientHttpRequestFactory());

        String url = "/data/services/tokens/issue";

        System.out.println("Get alias token at " + new Date());
        HttpEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        HttpHeaders headers = response.getHeaders();
        List<String> cookies = headers.get( HttpHeaders.SET_COOKIE);
        HttpCookie sessionCookie = getCookieWithName( cookies, "JSESSIONID");
        HttpCookie expirationCookie = getCookieWithName( cookies, "SESSION_EXPIRATION_TIME");

        // response always is of type text/plain so auto mapping to converter doesn't happen.
        String aliasTokenString = response.getBody();
        JsonNode node = (new ObjectMapper()).readTree( aliasTokenString);
        AliasToken aliasToken = new AliasToken();
        aliasToken.setAlias( node.get("alias").asText( restUser));
        aliasToken.setSecret( node.get("secret").asText( restPwd));

        xnatSession.setAliasToken( aliasToken);
        xnatSession.setjSessionCookie( sessionCookie);
        xnatSession.setSessionExpirationCookie( expirationCookie);
        return xnatSession;
    }

    private HttpCookie getCookieWithName( List<String> rawCookies, String name) {
        HttpCookie cookie = null;
        for( String rawCookie: rawCookies) {
            if( rawCookie.contains(name)) {
                cookie = HttpCookie.parse( rawCookie).get(0);
                break;
            }
        }
        return cookie;
    }

    private List<HttpMessageConverter<?>> getMessageConverters() {
        List<HttpMessageConverter<?>> converters = new ArrayList<>();

//        XStreamMarshaller marshaller = new XStreamMarshaller();
        Jaxb2Marshaller marshaller = createJaxb2Marshaller();
        MarshallingHttpMessageConverter marshallingConverter = new MarshallingHttpMessageConverter(marshaller);
        converters.add(marshallingConverter);

        converters.add(new MappingJackson2HttpMessageConverter());

        converters.add(new StringHttpMessageConverter());

        return converters;
    }

    private Jaxb2Marshaller createJaxb2Marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//        marshaller.setContextPath("org.nrg.wr");
//        marshaller.setClassesToBeBound(
//                org.nrg.wr.SubjectData.class
//        );
        marshaller.setPackagesToScan("org.nrg.xnat.datamodel", "org.nrg.catalog");
        marshaller.setCheckForXmlRootElement(false);
        marshaller.setSupportJaxbElementClass(true);
        marshaller.setAdapters( new CalendarAdaptor());
        //marshaller.setMarshallerProperties(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        return marshaller;
    }

    protected class DateAdapter extends XmlAdapter<String, Date> {

        private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        @Override
        public String marshal(Date v) throws Exception {
            synchronized (dateFormat) {
                return dateFormat.format(v);
            }
        }

        @Override
        public Date unmarshal(String v) throws Exception {
            synchronized (dateFormat) {
                return dateFormat.parse(v);
            }
        }
    }

//    protected class XmlDateAdapter extends XmlAdapter<String, XMLGregorianCalendar> {
//
//        private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//
//        @Override
//        public String marshal(Date v) throws Exception {
//            synchronized (dateFormat) {
//                return dateFormat.format(v);
//            }
//        }
//
//        @Override
//        public Date unmarshal(String v) throws Exception {
//            synchronized (dateFormat) {
//                return dateFormat.parse(v);
//            }
//        }
//    }

    protected class CalendarAdaptor extends XmlAdapter<String, XMLGregorianCalendar> {

        @Override
        public String marshal(XMLGregorianCalendar v) throws Exception {
            return v.getYear()+"-"+v.getMonth()+"-"+v.getDay();
        }

        @Override
        public XMLGregorianCalendar unmarshal(String v) throws Exception {
            return null;
        }
    }
}
