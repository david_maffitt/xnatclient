package org.nrg.xnatClient;

import java.net.HttpCookie;

/**
 * Collect the objects that define an XNAT REST session.
 *
 */
public class XNATSession {
    private AliasToken aliasToken;
    private HttpCookie jSessionCookie;
    private HttpCookie sessionExpirationCookie;

    public AliasToken getAliasToken() {
        return aliasToken;
    }

    public void setAliasToken(AliasToken aliasToken) {
        this.aliasToken = aliasToken;
    }

    public HttpCookie getjSessionCookie() {
        return jSessionCookie;
    }

    public void setjSessionCookie(HttpCookie jSessionCookie) {
        this.jSessionCookie = jSessionCookie;
    }

    public HttpCookie getSessionExpirationCookie() {
        return sessionExpirationCookie;
    }

    public void setSessionExpirationCookie(HttpCookie sessionExpirationCookie) {
        this.sessionExpirationCookie = sessionExpirationCookie;
    }

    public String getJSessionID() {
        return jSessionCookie.getValue();
    }
}
