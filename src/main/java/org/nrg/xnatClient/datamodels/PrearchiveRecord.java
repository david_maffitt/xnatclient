package org.nrg.xnatClient.datamodels;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrearchiveRecord {
    private String projectID;
    private String timestamp;
    private Date lastmod;
    private Date uploaded;
    private Date scan_date;
    private Date scan_time;
    private String subjectLabel;
    private String folderName;
    private String name;
    private String tag;
    private String status;
    private String url;
    private String autoarchive;
    private boolean prevent_anon;
    private boolean prevent_auto_commit;
    private String source;
    private String visit;
    private String protocol;
    private String timezone;

    private static DateFormat df = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss.SSS");

    public PrearchiveRecord( String csvString) {
        String[] tokens = csvString.split(",", -1);
        if( tokens.length != 19) {
            throw new IllegalArgumentException("Too few tokens in PrearchiveRecord csv: " + csvString);
        }
        try {
            projectID = tokens[0];
            timestamp = tokens[1];
            lastmod = df.parse(tokens[2]);
            uploaded = df.parse(tokens[3]);
            scan_date = df.parse(tokens[4]);
            scan_time = null;
            subjectLabel = tokens[6];
            folderName = tokens[7];
            name = tokens[8];
            tag = tokens[9];
            status = tokens[10];
            url = tokens[11];
            autoarchive = tokens[12];
            prevent_anon = tokens[13].toLowerCase().equals("true");
            prevent_auto_commit = tokens[14].toLowerCase().equals("true");
            source = tokens[15];
            visit = tokens[16];
            protocol = tokens[17];
            timezone = tokens[18];
        }
        catch( ParseException e) {
            throw new IllegalArgumentException("Bad PrearchiveRecord csv format: " + e.getMessage() + ". csvString is " + csvString);
        }
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Date getLastmod() {
        return lastmod;
    }

    public void setLastmod(Date lastmod) {
        this.lastmod = lastmod;
    }

    public Date getUploaded() {
        return uploaded;
    }

    public void setUploaded(Date uploaded) {
        this.uploaded = uploaded;
    }

    public Date getScan_date() {
        return scan_date;
    }

    public void setScan_date(Date scan_date) {
        this.scan_date = scan_date;
    }

    public Date getScan_time() {
        return scan_time;
    }

    public void setScan_time(Date scan_time) {
        this.scan_time = scan_time;
    }

    public String getSubjectLabel() {
        return subjectLabel;
    }

    public void setSubjectLabel(String subjectLabel) {
        this.subjectLabel = subjectLabel;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAutoarchive() {
        return autoarchive;
    }

    public void setAutoarchive(String autoarchive) {
        this.autoarchive = autoarchive;
    }

    public boolean isPrevent_anon() {
        return prevent_anon;
    }

    public void setPrevent_anon(boolean prevent_anon) {
        this.prevent_anon = prevent_anon;
    }

    public boolean isPrevent_auto_commit() {
        return prevent_auto_commit;
    }

    public void setPrevent_auto_commit(boolean prevent_auto_commit) {
        this.prevent_auto_commit = prevent_auto_commit;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getVisit() {
        return visit;
    }

    public void setVisit(String visit) {
        this.visit = visit;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
