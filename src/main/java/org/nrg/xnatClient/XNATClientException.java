package org.nrg.xnatClient;

public class XNATClientException extends Exception {
    public XNATClientException(String msg) {
        super(msg);
    }
    public XNATClientException(String msg, Exception e) {
        super( msg, e);
    }
}
