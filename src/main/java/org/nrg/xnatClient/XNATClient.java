package org.nrg.xnatClient;

import org.nrg.catalog.Catalog;
import org.nrg.xnat.datamodel.*;
import org.nrg.xnatClient.datamodels.PrearchiveRecord;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;

import javax.sql.DataSource;
import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.namespace.QName;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class XNATClient extends XNATClientBase {

    public XNATClient(DataSource dataSource, String restURL, String restUser, String restPwd) throws IOException {
        super( dataSource,  restURL,  restUser,  restPwd);
    }

    /**
     * Create subject. Will return ID of new subject or pre-existing one.
     *
     * @param projectID The project ID of the project owning the new subject.
     * @param subjectLabel The label by which the new subject will be known.
     * @return subjectID The internal XNAT ID for the subject.
     *
     * @throws RestClientException
     */
    public String createSubject( String projectID, String subjectLabel) throws RestClientException {

        String subjectURL = "/data/projects/{project-id}/subjects/{subject-label}";

        System.out.println("Create subject '"+ projectID + ":" +subjectLabel + "' at " + new Date());

        ResponseEntity<String> response = restTemplate.exchange(subjectURL, HttpMethod.PUT, null, String.class, projectID, subjectLabel);
        String subjectID = response.getBody().toString();
        return subjectID;
    }

    public String getSubjectAsString( String projectID, String subjectLabel) throws RestClientException {

        String subjectURL = "/data/projects/{project-id}/subjects/{subject-label}?format=json";

        System.out.println("Get subject '"+ projectID + ":" +subjectLabel + "' at " + new Date());
        String result = restTemplate.getForObject( subjectURL, String.class, projectID, subjectLabel);
        return result;
    }

    public SubjectData getSubject(String projectID, String subjectLabel) throws RestClientException {

        String subjectURL = "/data/projects/{project-id}/subjects/{subject-label}?format=xml";

        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
        headers.setAccept( new ArrayList<>( Arrays.asList(MediaType.APPLICATION_XML, MediaType.TEXT_XML)));
        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);

//        System.out.println("Get subject '"+ projectID + ":" +subjectLabel + "' at " + new Date());
        ResponseEntity<JAXBElement> responseEntity = null;
        try {
            responseEntity= restTemplate.exchange(subjectURL, HttpMethod.GET, requestEntity, JAXBElement.class, projectID, subjectLabel);
        }
        catch( HttpClientErrorException e) {
            if( e.getStatusCode().is4xxClientError()) {
                return null;
            }
            else {
                throw e;
            }
        }
        return (SubjectData) responseEntity.getBody().getValue();
    }

    public SubjectData getSubjectByID( String subjectID) throws RestClientException {

        String subjectURL = "/data/subjects/{subject-ID}?format=xml";

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept( new ArrayList<>( Arrays.asList(MediaType.APPLICATION_XML, MediaType.TEXT_XML)));
        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);

        ResponseEntity<JAXBElement> responseEntity = null;
        try {
            responseEntity= restTemplate.exchange(subjectURL, HttpMethod.GET, requestEntity, JAXBElement.class, subjectID);
        }
        catch( HttpClientErrorException e) {
            if( e.getStatusCode().is4xxClientError()) {
                return null;
            }
            else {
                throw e;
            }
        }
        return (SubjectData) responseEntity.getBody().getValue();
    }

    public void setSubjectDemographics( String projectID, String subjectID, DemographicData demographicData) {
        String subjectURL = "/data/projects/{project-id}/subjects/{subject-label}?inbody=true";

        SubjectData subjectData = getSubjectByID( subjectID);
        if( subjectData != null) {
            subjectData.setDemographics(demographicData);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.TEXT_XML);

            QName qName = getQName( subjectData.getClass());
            JAXBElement<? extends ExperimentData> body = new JAXBElement(qName, subjectData.getClass(), subjectData);
            HttpEntity<JAXBElement<? extends ExperimentData>> entity = new HttpEntity<JAXBElement<? extends ExperimentData>>(body, headers);

            ResponseEntity<String> response = restTemplate.exchange( subjectURL, HttpMethod.PUT, entity, String.class, projectID, subjectID);
        }
    }

    public Optional<DemographicData> getDemographicData( String subjectID) {
        DemographicData demographicData = null;
        SubjectData subjectData = getSubjectByID( subjectID);
        if( subjectData != null) {
            AbstractDemographicData abstractDemographicData = subjectData.getDemographics();
            demographicData = null;
            if (abstractDemographicData != null) {
                if (abstractDemographicData instanceof DemographicData) {
                    demographicData = (DemographicData) abstractDemographicData;
                } else {
                    String msg = "Unknown abstractDemographicsData type: " + abstractDemographicData.getClass();
                    throw new RuntimeException(msg);
                }
            }
        }
        return Optional.ofNullable( demographicData);
    }

    public List<String> getSubjectLabels( String projectLabel) {

        String sql = "select sd.id from xnat_subjectdata sd where sd.project = ?";
        RowMapper<String> mapper = (rs, row) -> {
            return rs.getString("id");
        };
        List<String> subjectLabels = jdbcTemplate.query(sql, mapper, projectLabel);
        return subjectLabels;
    }

    public List<String> getSubjectLabels() {

        String sql = "select sd.id from xnat_subjectdata sd ";
        RowMapper<String> mapper = (rs, row) -> {
            return rs.getString("id");
        };
        List<String> subjectLabels = jdbcTemplate.query(sql, mapper);
        return subjectLabels;
    }

    public List<String> getProjectLabels() {

        String sql = "select prj.name from xnat_projectdata prj";
        RowMapper<String> mapper = (rs, row) -> {
            return rs.getString("name");
        };
        List<String> labels = jdbcTemplate.query(sql, mapper);
        return labels;
    }

    public int getSubjectCount( String projectName) {
        String sql = "select count(*) subjectcount from xnat_subjectdata where xnat_subjectdata.project = ?;";
        return jdbcTemplate.queryForObject( sql, Integer.class, new Object[] {projectName});
    }

    public int getSessionCount( String projectName) {
        String sql = "select count(sess.id) sessioncount from xnat_imagesessiondata sess\n" +
                "join xnat_subjectassessordata sa on sess.id = sa.id\n" +
                "join xnat_experimentdata xe on sa.id = xe.id\n" +
                "join xnat_subjectdata sd on sa.subject_id = sd.id\n" +
                "where sd.project = ?";
        return jdbcTemplate.queryForObject( sql, Integer.class, new Object[] {projectName});
    }

    public Map<String, Integer> getSubjectCountPerStudyInstanceUID( String projectName) {
        String sql = "select sess.uid, count( distinct x.id) subjectcount from xnat_imagesessiondata sess\n" +
                "join xnat_subjectassessordata xs on sess.id = xs.id\n" +
                "join xnat_experimentdata xe on xs.id = xe.id\n" +
                "join xnat_subjectdata x on xs.subject_id = x.id\n" +
                "where x.project = ?\n" +
                "group by sess.uid\n" +
                "order by subjectcount desc;";

        ResultSetExtractor< Map<String,Integer>> extractor = new ResultSetExtractor<Map<String,Integer>>(){
            @Override
            public Map<String, Integer> extractData(ResultSet rs) throws SQLException {
                HashMap<String,Integer> mapRet = new HashMap<>();
                while(rs.next()){
                    mapRet.put(rs.getString("uid"),rs.getInt("subjectcount"));
                }
                return mapRet;
            }
        };

        return jdbcTemplate.query(sql, extractor, new Object[] {projectName});
    }

    public Map<String, Integer> getProjectCountPerStudyInstanceUID() {
        String sql = "select isd.uid, count(project) projectcount from xnat_experimentdata exd\n" +
                "left join xnat_imagesessiondata isd on isd.id = exd.id\n" +
                "where uid is not null\n" +
                "group by uid\n" +
                "having count(project) > 1\n" +
                "order by count(project) desc ;";

        ResultSetExtractor< Map<String,Integer>> extractor = new ResultSetExtractor<Map<String,Integer>>(){
            @Override
            public Map<String, Integer> extractData(ResultSet rs) throws SQLException {
                HashMap<String,Integer> mapRet = new HashMap<>();
                while(rs.next()){
                    mapRet.put(rs.getString("uid"),rs.getInt("projectcount"));
                }
                return mapRet;
            }
        };

        return jdbcTemplate.query(sql, extractor);
    }

    public List<String> getProjectLabelsByStudyInstanceUID( String studyInstanceUID) {
        String sql = "select project as projectlabel from xnat_experimentdata exd\n" +
                "left join xnat_imagesessiondata isd on isd.id = exd.id\n" +
                "where uid = ?\n" +
                "order by projectlabel asc;";
        ResultSetExtractor< List<String>> extractor = new ResultSetExtractor<List<String>>(){
            @Override
            public List<String> extractData(ResultSet rs) throws SQLException {
                ArrayList<String> list = new ArrayList<>();
                while(rs.next()){
                    list.add(rs.getString("projectlabel"));
                }
                return list;
            }
        };

        return jdbcTemplate.query(sql, extractor, new Object[] {studyInstanceUID});
    }

    public Properties getSesssionIdentifiers( String projectLabel, String studyInstanceUID) {
        String sql = "select exd.id sessionid, exd.label sessionlabel from xnat_experimentdata exd\n" +
                "left join xnat_imagesessiondata isd on isd.id = exd.id\n" +
                "where uid = ?\n" +
                "and project = ?;";
        ResultSetExtractor<Properties> extractor = new ResultSetExtractor< Properties>(){
            @Override
            public Properties extractData(ResultSet rs) throws SQLException {
                Properties properties = new Properties();
                while(rs.next()){
                    properties.setProperty( "sessionid", rs.getString("sessionid"));
                    properties.setProperty( "sessionlabel", rs.getString("sessionlabel"));
                }
                return properties;
            }
        };

        return jdbcTemplate.query(sql, extractor, new Object[] { studyInstanceUID, projectLabel});
    }

    public Properties getDicomwebScanAttributes( String sessionLabel, String scanID) {
        String sql = "select distinct xe.date study_date, xe.time study_time, sess.dcmaccessionnumber accession_number,\n" +
                "    sess.dcmpatientname patient_name, sess.dcmpatientname patient_id,\n" +
                "--        sess.referring_physician referring_physician,\n" +
                "    sess.uid study_instance_uid, sess.study_id study_id, xd.gender gender, xd.dob dob,\n" +
                "    scan.modality modality, scan.series_description series_description, scan.uid series_instance_uid,\n" +
                "-- scan.series_number series_number,\n" +
                "from xnat_imagescandata scan\n" +
                "left join xnat_imagesessiondata sess on scan.image_session_id = sess.id\n" +
                "left join xnat_subjectassessordata subass on sess.id = subass.id\n" +
                "left join xnat_experimentdata xe on subass.id = xe.id\n" +
                "left join xnat_subjectdata subjd on subass.subject_id = subjd.id\n" +
                "left join xnat_abstractdemographicdata xa on subjd.demographics_xnat_abstractdemographicdata_id = xa.xnat_abstractdemographicdata_id\n" +
                "left join xnat_demographicdata xd on xa.xnat_abstractdemographicdata_id = xd.xnat_abstractdemographicdata_id\n" +
                "where xe.label = ?\n" +
                "and scan.id = ?\n" +
                "group by study_date, study_time, accession_number, patient_name, patient_id, study_instance_uid, study_id, gender, dob,\n" +
                "         scan.modality, series_description, series_instance_uid;";
        ResultSetExtractor<Properties> extractor = new ResultSetExtractor< Properties>(){
            @Override
            public Properties extractData(ResultSet rs) throws SQLException {
                Properties properties = new Properties();
                while(rs.next()){
                    properties.setProperty( "study_date", rs.getString("study_date"));
                    properties.setProperty( "study_time", rs.getString("study_time"));
                    properties.setProperty( "accession_number", rs.getString("accession_number"));
                    properties.setProperty( "patient_name", rs.getString("patient_name"));
                    properties.setProperty( "patient_id", rs.getString("patient_id"));
                    properties.setProperty( "study_instance_uid", rs.getString("study_instance_uid"));
                    properties.setProperty( "study_id", rs.getString("study_id"));
                    properties.setProperty( "study_time", rs.getString("study_time"));
                    properties.setProperty( "gender", rs.getString("gender"));
                    properties.setProperty( "dob", rs.getString("dob"));
                    properties.setProperty( "modality", rs.getString("modality"));
                    properties.setProperty( "series_description", rs.getString("series_description"));
                    properties.setProperty( "series_instance_uid", rs.getString("series_instance_uid"));
//                    properties.setProperty( "sop_class_uid", rs.getString("sop_class_uid"));
//                    properties.setProperty( "sop_instance_uid", rs.getString("sop_instance_uid"));
//                    properties.setProperty( "instance_number", rs.getString("instance_number"));
//                    properties.setProperty( "rows", rs.getString("rows"));
//                    properties.setProperty( "cols", rs.getString("cols"));
//                    properties.setProperty( "bits_allocated", rs.getString("bits_allocated"));
//                    properties.setProperty( "frame_count", rs.getString("frame_count"));
                }
                return properties;
            }
        };

        return jdbcTemplate.query(sql, extractor, new Object[] { sessionLabel, scanID});
    }

    public Properties getDicomwebAttributes( String sessionLabel, String scanID, int instanceNumber) {
        String sql = "select distinct xe.date study_date, xe.time study_time, sess.dcmaccessionnumber accession_number,\n" +
                "    sess.dcmpatientname patient_name, sess.dcmpatientname patient_id,\n" +
                "--        sess.referring_physician referring_physician,\n" +
                "    sess.uid study_instance_uid, sess.study_id study_id, xd.gender gender, xd.dob dob,\n" +
                "    scan.modality modality, scan.series_description series_description, scan.uid series_instance_uid,\n" +
                "-- scan.series_number series_number,\n" +
                "    di.sopclassuid sop_class_uid, di.sopinstanceuid sop_instance_uid, di.instancenumber instance_number, di.rows, di.cols, di.bits_allocated,\n" +
                "    count(df.framenumber) as frame_count\n" +
                "from xnat_imagescandata scan\n" +
                "left join xnat_imagesessiondata sess on scan.image_session_id = sess.id\n" +
                "left join xnat_subjectassessordata subass on sess.id = subass.id\n" +
                "left join xnat_experimentdata xe on subass.id = xe.id\n" +
                "left join xnat_subjectdata subjd on subass.subject_id = subjd.id\n" +
                "left join xnat_abstractdemographicdata xa on subjd.demographics_xnat_abstractdemographicdata_id = xa.xnat_abstractdemographicdata_id\n" +
                "left join xnat_demographicdata xd on xa.xnat_abstractdemographicdata_id = xd.xnat_abstractdemographicdata_id\n" +
                "left join dicominstance di on scan.id = di.scanid\n" +
                "left join dicomframe df on di.id = df.fk_instance_id\n" +
                "where xe.label = ?\n" +
                "and scan.id = ?\n" +
                "and di.instancenumber = ?\n" +
                "group by study_date, study_time, accession_number, patient_name, patient_id, study_instance_uid, study_id, gender, dob,\n" +
                "         scan.modality, series_description, series_instance_uid, sop_class_uid, sop_instance_uid, instance_number, rows, cols, bits_allocated;";
        ResultSetExtractor<Properties> extractor = new ResultSetExtractor< Properties>(){
            @Override
            public Properties extractData(ResultSet rs) throws SQLException {
                Properties properties = new Properties();
                while(rs.next()){
                    properties.setProperty( "study_date", rs.getString("study_date"));
                    properties.setProperty( "study_time", rs.getString("study_time"));
                    properties.setProperty( "accession_number", rs.getString("accession_number"));
                    properties.setProperty( "patient_name", rs.getString("patient_name"));
                    properties.setProperty( "patient_id", rs.getString("patient_id"));
                    properties.setProperty( "study_instance_uid", rs.getString("study_instance_uid"));
                    properties.setProperty( "study_id", rs.getString("study_id"));
                    properties.setProperty( "study_time", rs.getString("study_time"));
                    properties.setProperty( "gender", rs.getString("gender"));
                    properties.setProperty( "dob", rs.getString("dob"));
                    properties.setProperty( "modality", rs.getString("modality"));
                    properties.setProperty( "series_description", rs.getString("series_description"));
                    properties.setProperty( "series_instance_uid", rs.getString("series_instance_uid"));
                    properties.setProperty( "sop_class_uid", rs.getString("sop_class_uid"));
                    properties.setProperty( "sop_instance_uid", rs.getString("sop_instance_uid"));
                    properties.setProperty( "instance_number", rs.getString("instance_number"));
                    properties.setProperty( "rows", rs.getString("rows"));
                    properties.setProperty( "cols", rs.getString("cols"));
                    properties.setProperty( "bits_allocated", rs.getString("bits_allocated"));
                    properties.setProperty( "frame_count", rs.getString("frame_count"));
                }
                return properties;
            }
        };

        return jdbcTemplate.query(sql, extractor, new Object[] { sessionLabel, scanID, instanceNumber});
    }

    public SubjectData getSubjectAsObject(String projectID, String subjectLabel) throws RestClientException {

        String subjectURL = "/data/projects/{project-id}/subjects/{subject-label}?format=xml";

        System.out.println("Get subject '"+ projectID + ":" +subjectLabel + "' at " + new Date());
        SubjectData result = restTemplate.getForObject( subjectURL, SubjectData.class, projectID, subjectLabel);
        return result;
    }

    /**
     * Delete the subject.
     *
     * Throws HttpClientErrorException: 404 Not Found if not found.
     *
     * @param projectID
     * @param subjectLabel
     * @throws RestClientException
     * @return true if deleted, false if subject was not found.
     */
    public boolean deleteSubject( String projectID, String subjectLabel) throws RestClientException {

        String subjectURL = "/data/projects/{project-id}/subjects/{subject-label}";

        System.out.println("Delete subject '"+ projectID + ":" +subjectLabel + "' at " + new Date());
        try {
            Instant start = Instant.now();
            restTemplate.delete(subjectURL, projectID, subjectLabel);
            Instant stop = Instant.now();
            System.out.println("Subject deleted in " + (ChronoUnit.MILLIS.between(start, stop) / 1000.0) + " secs.");

            return true;
        }
        catch ( HttpClientErrorException e) {
            if( e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return false;
            }
            throw e;
        }
    }

    public void deleteSubjects( String projectID, List<String> subjectLabels) {

        for( String subjectLabel: subjectLabels) {
            try {
                deleteSubject(projectID, subjectLabel);
            }
            catch( RestClientException e) {
                System.out.println("Failed to delete subject '" + subjectLabel + "': " + e.getMessage());
            }
        }
    }

    public void createExperiment( String projectID, String subjectLabel, String experimentID, String experiment) {
        String experimentURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-id}?allowDataDeletion=true&inbody=true";
        System.out.println("Create experiment '"+ projectID + ":" +subjectLabel + ":" + experimentID + "' at " + new Date());

        String requestBody = experiment;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);

        ResponseEntity<String> response = restTemplate.exchange( experimentURL, HttpMethod.PUT, entity, String.class, projectID, subjectLabel, experimentID);

        if( response.getStatusCode().is2xxSuccessful()) {
            System.out.println("Woo hoo! experiment ID is " + response.getBody());
        }
        else {
            System.out.println("Boo hoo! Response status is " + response.getStatusCode());
        }
    }

    public void createExperiment( String projectID, String subjectLabel, String experimentID, ExperimentData experiment) {
        String experimentURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-id}?allowDataDeletion=true&inbody=true";
        System.out.println("Create experiment '"+ projectID + ":" +subjectLabel + ":" + experimentID + "' at " + new Date());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);

//        QName qName = new QName( "http://nrg.wustl.edu/xnat", "Experiment", "xnat");
        QName qName = getQName( experiment.getClass());
        JAXBElement<? extends ExperimentData> body = new JAXBElement(qName, experiment.getClass(), experiment);
        HttpEntity<JAXBElement<? extends ExperimentData>> entity = new HttpEntity<JAXBElement<? extends ExperimentData>>(body, headers);
//        HttpEntity<ExperimentData> entity = new HttpEntity<>(experiment, headers);

        ResponseEntity<String> response = restTemplate.exchange( experimentURL, HttpMethod.PUT, entity, String.class, projectID, subjectLabel, experimentID);

        if( response.getStatusCode().is2xxSuccessful()) {
            System.out.println("Woo hoo! experiment ID is " + response.getBody());
        }
        else {
            System.out.println("Boo hoo! Response status is " + response.getStatusCode());
        }
    }

    public String createSubjectAssessor(SubjectAssessorData assessor) {
        String experimentURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-id}?allowDataDeletion=true&inbody=true";
        String projectID = assessor.getProject();
        String subjectLabel = assessor.getSubjectID();
        String experimentLabel = assessor.getLabel();

                System.out.println("Create experiment '"+ projectID + ":" +subjectLabel + ":" + experimentLabel + "' at " + new Date());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_XML);

        QName qName = getQName( assessor.getClass());
        JAXBElement<? extends ExperimentData> body = new JAXBElement(qName, assessor.getClass(), assessor);
        HttpEntity<JAXBElement<? extends ExperimentData>> entity = new HttpEntity<JAXBElement<? extends ExperimentData>>(body, headers);

        ResponseEntity<String> response = restTemplate.exchange( experimentURL, HttpMethod.PUT, entity, String.class, projectID, subjectLabel, experimentLabel);
        String experimentID = response.getBody().toString();
        return experimentID;
    }

    public String updateSubjectAssessor( SubjectAssessorData assessor) {
        return createSubjectAssessor( assessor);
    }

    public String getExperimentAsString( String projectID, String subjectLabel, String experimentLabel) throws RestClientException {

        String experimentURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-label}?format=json";

        System.out.println("Get experiment '"+ projectID + ":" +subjectLabel + ":" + experimentLabel + "' at " + new Date());
        String result = restTemplate.getForObject( experimentURL, String.class, projectID, subjectLabel, experimentLabel);
        return result;
    }

    public ExperimentData getExperiment( String projectID, String subjectLabel, String experimentLabel) throws RestClientException {

        String experimentURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-label}?format=xml";

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept( new ArrayList<>( Arrays.asList(MediaType.APPLICATION_XML, MediaType.TEXT_XML)));
        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);

        System.out.println("Get experiment '"+ projectID + ":" +subjectLabel + ":" + experimentLabel + "' at " + new Date());
        HttpEntity<JAXBElement> resultEntity = restTemplate.exchange( experimentURL, HttpMethod.GET, requestEntity, JAXBElement.class, projectID, subjectLabel, experimentLabel);

        return (ExperimentData) resultEntity.getBody().getValue();
    }

    public void deleteExperiment( String projectID, String subjectLabel, String experimentLabel) throws RestClientException {

        String experimentURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-label}";

        System.out.println("Delete experiment '"+ projectID + ":" +subjectLabel + ":" + experimentLabel + "' at " + new Date());
        restTemplate.delete( experimentURL, projectID, subjectLabel, experimentLabel);
    }

    public enum ScanType {
        DICOM, SECONDARY, UNKNOWN;
    }

    public Catalog getScanCatalog(String projectID, String subjectLabel, String experimentLabel, String scanLabel) {

        return getScanCatalog( projectID, subjectLabel, experimentLabel, scanLabel, ScanType.DICOM);
    }

    public ScanType getScanType(ImageScanData scanData) {
        ScanType scanType = ScanType.UNKNOWN;
        List<AbstractResource> scanDataFiles = scanData.getFile();
        for( AbstractResource resource: scanDataFiles) {
            if( "DICOM".equalsIgnoreCase(resource.getLabel())) {
                scanType = ScanType.DICOM;
                break;
            }
            else if( "SECONDARY".equalsIgnoreCase(resource.getLabel())) {
                scanType = ScanType.SECONDARY;
                break;
            }
        }
        return scanType;
    }

    /* We can't rely on XNAT to always use DCMCatalog.  Sometimes it is just a Catalog. */
//    public DcmCatalog getScanCatalog(String projectID, String subjectLabel, String experimentLabel, String scanLabel, ScanType scanType) {
//
//        String catalogURL;
//        switch (scanType) {
//            case SECONDARY:
//                catalogURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-label}/scans/{scan-id}/resources/secondary";
//                break;
//            case DICOM:
//            case UNKNOWN:
//            default:
//                catalogURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-label}/scans/{scan-id}/resources/DICOM";
//                break;
//        }
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setAccept( new ArrayList<>( Arrays.asList(MediaType.APPLICATION_XML, MediaType.TEXT_XML)));
//        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);
//
//        System.out.println("Get scan catalog '"+ projectID + ":" +subjectLabel + ":" + experimentLabel + ":" + scanLabel + "' at " + new Date());
//        HttpEntity<JAXBElement> resultEntity = restTemplate.exchange( catalogURL, HttpMethod.GET, requestEntity, JAXBElement.class, projectID, subjectLabel, experimentLabel, scanLabel);
//
//        return (DcmCatalog) resultEntity.getBody().getValue();
//    }

    public Catalog getScanCatalog(String projectID, String subjectLabel, String experimentLabel, String scanLabel, ScanType scanType) {

        String catalogURL;
        switch (scanType) {
            case SECONDARY:
                catalogURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-label}/scans/{scan-id}/resources/secondary";
                break;
            case DICOM:
            case UNKNOWN:
            default:
                catalogURL = "/data/projects/{project-id}/subjects/{subject-label}/experiments/{experiment-label}/scans/{scan-id}/resources/DICOM";
                break;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept( new ArrayList<>( Arrays.asList(MediaType.APPLICATION_XML, MediaType.TEXT_XML)));
        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);

        System.out.println("Get scan catalog '"+ projectID + ":" +subjectLabel + ":" + experimentLabel + ":" + scanLabel + "' at " + new Date());
        HttpEntity<JAXBElement> resultEntity = restTemplate.exchange( catalogURL, HttpMethod.GET, requestEntity, JAXBElement.class, projectID, subjectLabel, experimentLabel, scanLabel);
        Catalog catalog = (org.nrg.catalog.Catalog) resultEntity.getBody().getValue();
        return catalog;
    }

    public void setSubjectCustomVariable(String projectID, String subjectLabel, String variableName, String value) {

        String resourceURL = "/data/archive/projects/{project-id}/subjects/{subject-label}?xnat:subjectData/fields/field[name={variableName}]/field={value}";
        //curl -k -u gkgurneyAdmin -X PUT "https://cap.wustl.edu/data/archive/projects/BC_1/subjects/bc1s5kh?xnat:subjectData/fields/field%5Bname%3Dhr_case%5D/field=true"

        System.out.println("Set subject custom variable: '" + projectID + ":" +subjectLabel + ":" + variableName + "=" + value + "' at " + new Date());

        ResponseEntity<String> response = restTemplate.exchange(resourceURL, HttpMethod.PUT, null, String.class, projectID, subjectLabel, variableName, value);
//        String subjectID = response.getBody().toString();
        return;
    }

    public void setSubjectCustomVariables(String projectID, String subjectLabel, Map<String,String> map) {

        //curl -k -u gkgurneyAdmin -X PUT "https://cap.wustl.edu/data/archive/projects/BC_1/subjects/bc1s5kh?xnat:subjectData/fields/field%5Bname%3Dhr_case%5D/field=true"
        MessageFormat urlRootFormat = new MessageFormat("/data/archive/projects/{0}/subjects/{1}");
//        MessageFormat queryParamFormat = new MessageFormat("xnat:subjectData/fields/field%5Bname%3D{0}%5D/field={1}");
        MessageFormat queryParamFormat = new MessageFormat("xnat:subjectData/fields/field[name={0}]/field={1}");

        boolean first = true;
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append( urlRootFormat.format( new Object[]{projectID, subjectLabel}));
        for( String name: map.keySet()) {
            if( first) {
                urlBuilder.append("?");
                first = false;
            }
            else {
                urlBuilder.append("&");
            }
            urlBuilder.append( queryParamFormat.format( new Object[]{name, map.get(name)}));
        }
        String resourceURL = urlBuilder.toString();
        System.out.println("Set subject custom variable(s): '" + projectID + ":" + subjectLabel + "' at " + new Date());

        ResponseEntity<String> response = restTemplate.exchange(resourceURL, HttpMethod.PUT, null, String.class);
//        String subjectID = response.getBody().toString();
        return;
    }

    public String uploadImageSessionZip(File zipFile) throws XNATClientException {
//        curl -k -u admin:admin -X POST "https://cap-dev-dist1.nrg.mir/data/services/import?dest=/prearchive" -F "file=@bc1gtxx.zip"
//        "https://cnda.wustl.edu:443/data/services/import?import-handler=DICOM-zip&amp;inbody=true

//        String resourceURL = "/data/services/import?dest=/archive&overwrite=append";
        String resourceURL = "/data/services/import?import-handler=DICOM-zip&inbody=true";

        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("zip-file", new FileSystemResource( zipFile));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        System.out.println("Upload zip: '" + zipFile + "' at " + new Date());
        try {
            ResponseEntity<String> response = restTemplate.exchange(resourceURL,
                    HttpMethod.POST, requestEntity, String.class);
            System.out.println("Status: " + response.getStatusCode() + " " + response.getBody());
            return response.getBody();
        }
        catch( RestClientResponseException e) {
            String msg = MessageFormat.format("Error uploading session. Status {0} {1}: {2}", e.getRawStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            throw new XNATClientException( msg, e);
        }
    }

    public String uploadImageSessionZip(File zipFile, String projectLabel, String subjectLabel, String sessionLabel) throws XNATClientException {

        String resourceURL = MessageFormat.format("/data/services/import?dest=/archive/projects/{0}/subjects/{1}/experiments/{3}&overwrite=append", projectLabel, subjectLabel, sessionLabel);
//        String resourceURL = "/data/services/import?import-handler=DICOM-zip&inbody=true";

        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();
        bodyMap.add("zip-file", new FileSystemResource( zipFile));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        System.out.println("Upload zip: '" + zipFile + "' at " + new Date());
        try {
            ResponseEntity<String> response = restTemplate.exchange(resourceURL,
                    HttpMethod.POST, requestEntity, String.class);
            System.out.println("Status: " + response.getStatusCode() + " " + response.getBody());
            return response.getBody();
        }
        catch( RestClientResponseException e) {
            String msg = MessageFormat.format("Error uploading session. Status {0} {1}: {2}", e.getRawStatusCode(), e.getStatusText(), e.getResponseBodyAsString());
            throw new XNATClientException( msg, e);
        }
    }

    public List<PrearchiveRecord> readPrearchive( String projectID) {
        String resourceURL = MessageFormat.format("/data/prearchive/projects/{0}?format=csv", projectID);
//        ResponseEntity<List<String>> response = restTemplate.exchange(resourceURL, HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>(){});
        String response = restTemplate.getForObject(resourceURL, String.class);
        List<String> lines = getLines(response);
        List<PrearchiveRecord> records = new ArrayList<>();
        System.out.println( response);
//        for( String s: response.getBody()) {
//            PrearchiveRecord record = new PrearchiveRecord(s);
//            records.add( record);
//        }
        return records;
    }

    private List<String> getLines(String line) {
        return Arrays.asList( line.split("\n",-1));
    }

    private <T> QName getQName(final Class<T> clazz) {
        // No other way since it is not @RootXmlElement
        final String xmlns;
        final Package aPackage = clazz.getPackage();
        if (aPackage.isAnnotationPresent(XmlSchema.class)) {
            xmlns = aPackage.getDeclaredAnnotation(XmlSchema.class).namespace();
        } else {
            xmlns = ""; // May throw illegal
        }
        return new QName(xmlns, clazz.getSimpleName());
    }

}
