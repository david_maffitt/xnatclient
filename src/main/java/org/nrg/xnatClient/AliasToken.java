package org.nrg.xnatClient;

import java.util.Date;

public class AliasToken {
    private String alias;
    private String xdatUserId;
    private String secret;
    private boolean singleUse;
    private Date estimatedExpirationTime;
    private Date timestamp;
    private boolean enabled;
    private Date created;
    private int id;
    private int disabled;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getXdatUserId() {
        return xdatUserId;
    }

    public void setXdatUserId(String xdatUserId) {
        this.xdatUserId = xdatUserId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public boolean isSingleUse() {
        return singleUse;
    }

    public void setSingleUse(boolean singleUse) {
        this.singleUse = singleUse;
    }

    public Date getEstimatedExpirationTime() {
        return estimatedExpirationTime;
    }

    public void setEstimatedExpirationTime(Date estimatedExpirationTime) {
        this.estimatedExpirationTime = estimatedExpirationTime;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDisabled() {
        return disabled;
    }

    public void setDisabled(int disabled) {
        this.disabled = disabled;
    }

    @Override
    public String toString() {
        return "AliasToken{" +
                "alias='" + alias + '\'' +
                ", xdatUserId='" + xdatUserId + '\'' +
                ", secret='" + secret + '\'' +
                ", singleUse=" + singleUse +
                ", estimatedExpirationTime=" + estimatedExpirationTime +
                ", timestamp=" + timestamp +
                ", enabled=" + enabled +
                ", created=" + created +
                ", id=" + id +
                ", disabled=" + disabled +
                '}';
    }
}
