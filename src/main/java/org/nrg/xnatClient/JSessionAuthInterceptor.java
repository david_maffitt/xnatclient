package org.nrg.xnatClient;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

public class JSessionAuthInterceptor implements ClientHttpRequestInterceptor {
    private String jSessionID;

    public JSessionAuthInterceptor( String jSessionID) {
        this.jSessionID = jSessionID;
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        HttpHeaders requestHeaders = httpRequest.getHeaders();
        if(!requestHeaders.containsKey(HttpHeaders.COOKIE)) {
            requestHeaders.add("Cookie", "JSESSIONID=" + jSessionID);
        }

        return execution.execute(httpRequest, body);
    }
}
